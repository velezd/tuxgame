#include "map.h"

void Map::init(char *name) {
    path_map = complete_file_path((char*)folder_maps,name,(char*)suffix_maps);
    char buffer[400];
    char theme_name[50];
    int rows = 0;
    int lines = 0;
    FILE *file;
    file = fopen(path_map,"r");
    while (!feof(file)) {
        fgets(buffer, 400, file);
        lines++;
        if (lines == 1) {
            for (int i = 0; i < 50; i++) {
                if (buffer[i] != '\n')
                    theme_name[i] = buffer[i];
                else {
                    theme_name[i] = '\0';
                    i = 50;
                }
            }
        }
        else if (rows < strlen(buffer))
            rows = strlen(buffer);
    }
    fclose(file);
    width = rows-1;
    height = lines-2;
    path_textures = complete_file_path((char*)folder_textures,(char*)theme_name,(char*)suffix_textures);
    path_collision = complete_file_path((char*)folder_textures,(char*)theme_name,(char*)suffix_collision);
    path_background = complete_file_path((char*)folder_background,(char*)theme_name,(char*)suffix_textures);
}

void Map::load(Player *player) {
    // Prepare surface
    graphics = SDL_CreateRGBSurface(SDL_SWSURFACE,(width*32),(height*32),32,0,0,0,0);
    SDL_FillRect(graphics,NULL,SDL_MapRGBA(graphics->format,255,0,255,0));
    SDL_SetColorKey(graphics,SDL_SRCCOLORKEY,SDL_MapRGB(graphics->format,255,0,255));
    // Load Textures
    SDL_Surface *textures = NULL;
    textures = load_image(path_textures);
    // Load Collision of textures
    FILE *file;
    file = fopen(path_collision, "r");
    int i = 0;
    int buffer;
    int collision_buffer[17];
    while (buffer != EOF) {
        buffer = fgetc(file);
        if (buffer == '1')
            collision_buffer[i] = 1;
        else
            collision_buffer[i] = 0;
        i++;
    }
    // Load graphics
    fclose(file);
    file = NULL;
    file = fopen(path_map, "r");
    buffer = 0; 
    int x = 0;
    int y = 0;
    int rows = 0; // collision rows
    int lines = 0; // collision lines
    int ccount = 0; // count of coins created
    bool first_line = false;
    while (buffer != EOF) {
        buffer = fgetc(file);
        if (first_line == false) {
            if (buffer == '\n')
                first_line = true;
        }
        else {
            // Empty space
            if (buffer == '.') {
                x += 32;
                rows++;
            }
            // Creating map graphics and map collision array
            else if (buffer > 47 && buffer < 64) {
                blit_surface(textures,graphics,(buffer-48)*32,0,32,32,x,y);
                x += 32;
                collision[rows][lines] = collision_buffer[(buffer-48)];
                rows++;
            }
            // Initializing player
            else if (buffer == 'S') {
                player->init(x,y);
                x += 32;
                rows++;
            }
            // Creating coins
            else if (buffer == 'c') {
                coins[ccount].init(x,y,x/32,y/32);
                ccount++;
            }
            // New line
            else if (buffer == '\n') {
                x = 0;
                y += 32;
                rows = 0;
                lines++;
            }
        }
    }
    fclose(file);
    SDL_FreeSurface(textures);
}
