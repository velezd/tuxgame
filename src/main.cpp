#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "gamelib.h"
#include "camera.h"
#include "player.h"
#include "config.h"
#include "map.h"
#include "coin.h"

int main( int argc, char* args[] ) {
    init_sdl(); // Start SDL
    Player *player = new Player; // Create player
    
    char map_name[] = "first";
    
    Map *map = new Map;
    map->init(map_name);
    map->load(player); // Load map and init player

    Camera *camera = new Camera;
    //camera.init(player->x-(screen_width/2-(player->w/2)),player->y-(screen_height/2-(player->h/2)),screen_width,screen_height);
    camera->init(0,0,800,600);
    
    // Coin test
    Ccoin coin;
    coin.init(20,20,2,3);
    
    SDL_Surface *background = load_image(map->path_background); // load background
    
    // Variables
    Uint8 *keys;
    SDL_Event event;
    
    bool runs = true;
    while (runs) {
        keys = SDL_GetKeyState(NULL); // Get pressed keys
        
        blit_surface(background,screen,0,0,800,600,0,0);
        blit_surface(map->graphics,screen,camera->x,camera->y,camera->w,camera->h,0,0);
        
        player->control(keys);
        player->collision(map->collision);
        player->move(camera);
        player->draw(camera);
        coin.draw();
        
        SDL_Flip(screen);
        
        // Control game close
        if(keys[SDLK_ESCAPE])
            runs = false;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                runs = false;
            }
        }
    }

    SDL_Quit();
    return 0;
}
