#include "player.h"
    
void Player::init(int sx, int sy) { // player initialization
    character = load_image(file_player_skin);
    w = character->w/4;
    h = character->h;
    x = sx;
    y = sy;
    max_speed = 5;
    jump_lenght = 0;
}

void Player::control(Uint8 *keys) { // increasing velocity
    if (keys[SDLK_RIGHT] and velx < max_speed)
        velx = velx + 2;
    else if (keys[SDLK_LEFT] and velx > -max_speed)
        velx = velx - 2;
    else if (keys[SDLK_UP]) {
        if (jump_lenght >= 0) {
            vely -= 2;
        }
        jump_lenght++;
        std::cout << jump_lenght << "\n";
    }
    else { // if not pressed any key - slowing down
        if (velx > 0)
            velx--;
        if (velx < 0)
            velx++;
        if (vely > 0)
            vely--;
        if (vely < 0)
            vely += 5;
        if (vely == 0) {
            vely = game_gravity;
        }
    }
}

void Player::move(Camera *camera) { // moving player
    x = x + velx;
    y = y + vely;
    if (x > camera->w/2) {
        camera->x += velx;
    }
}

void Player::draw(Camera *camera) { // draw player
    blit_surface(character,screen,0,0,w,h,x-camera->x,y-camera->y);
}

void Player::collision(int collision[400][400]) { // solve collisions
    if (collision[(x+w+velx+1)/32][(y+h-1)/32] == 1 and velx > 0) {
        int temp = 0;
        while (collision[(x+w+temp+1)/32][(y+h-1)/32] == 0) {
            temp++;
        }
        velx = temp-1;
    }
    if (collision[(x+velx+1)/32][(y+h-1)/32] == 1 and velx < 0) {
        int temp = 0;
        while (collision[(x-temp+1)/32][(y+h-1)/32] == 0) {
            temp++;
        }
        velx = -temp+1;
    }
    if (collision[(x+w/2+1)/32][(y-vely)/32] == 1 and vely < 0) {
    int temp = 0;
        while (collision[(x+w/2+1)/32][(y-temp)/32] == 0) {
            temp++;
        }
        vely = -temp+1;    
    }
    if (collision[(x+w/2+1)/32][(y+h+vely)/32] == 1 and vely > 0) {
        int temp = 0;
        while (collision[(x+w/2+1)/32][(y+h+temp)/32] == 0) {
            temp++;
        }
        vely = temp-1;
    }
}
