#include "config.h"

// SDL
const int screen_width = 800;
const int screen_height = 600;
const int screen_bpp = 32;
const bool fullscreen = false;
const char *window_caption = "Game";
// Folders
const char *folder_maps = "./maps/";
const char *folder_textures = "./textures/";
const char *folder_background = "./textures/background/";
// Files
const char *file_player_skin = "./textures/player.png";
const char *file_coin_texture = "./textures/coin.png";  
// Suffixes
const char *suffix_maps = ".zmf";
const char *suffix_textures = ".png";
const char *suffix_collision = ".col";
// Game
const int game_gravity = 10;
