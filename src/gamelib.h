#ifndef _GAMELIB_H
#define _GAMELIB_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "config.h"
#include "player.h"

// Variables
extern SDL_Surface *screen;
extern SDL_Surface *coin_texture;
    
// Functions
void init_sdl();

char *complete_file_path(char *folder,char *filename,char *suffix);

SDL_Surface *load_image( std::string filename);

void blit_surface(SDL_Surface *from, SDL_Surface *to,int x1,int y1,int w,int h,int x2,int y2);

// Classes

class Ccoin {
    private:
        int x;
        int y;
        int collision[1];
    public:
    void init(int sx,int sy,int row,int line) {
        x = sx;
        y = sy;
        collision[0] = row;
        collision[1] = line;
    }
    void draw() {
        blit_surface(load_image(file_coin_texture),screen,0,0,32,32,x,y);
    }
};



#endif
