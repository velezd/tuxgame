#ifndef _MAP_H
#define _MAP_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "config.h"
#include "player.h"
#include "coin.h"
#include "gamelib.h"

class Map {
    private:
    char *path_map;
    char *path_textures;
    char *path_collision;
    int width;
    int height;
    
    public:
    SDL_Surface *graphics;
    char *path_background;
    int collision[400][400];
    
    void init(char *name);
    
    void load(Player*);
};

#endif
