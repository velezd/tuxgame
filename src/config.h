#ifndef _CONFIG_H
#define _CONFIG_H

extern const int screen_width;
extern const int screen_height;
extern const int screen_bpp;
extern const bool fullscreen;
extern const char *window_caption;
extern const char *folder_maps;
extern const char *folder_textures;
extern const char *folder_background;
extern const char *file_player_skin;
extern const char *file_coin_texture;
extern const char *suffix_maps;
extern const char *suffix_textures;
extern const char *suffix_collision;
extern const int game_gravity;

#endif
