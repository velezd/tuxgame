#include "coin.h"

coins[0] = NULL;

void Coin::init(int sx,int sy,int row,int line) {
    x = sx;
    y = sy;
    collision[0] = row;
    collision[1] = line;
}

void Coin::draw() {
    blit_surface(load_image(file_coin_texture),screen,0,0,32,32,x,y);
}
