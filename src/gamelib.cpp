#include "gamelib.h"

// Global
SDL_Surface *screen = NULL;
SDL_Surface *coin_texture = NULL;
//coin_texture = load_image(file_coin_texture);

// Init SDL
void init_sdl() {
    SDL_Init(SDL_INIT_EVERYTHING);
    if (fullscreen)
        screen = SDL_SetVideoMode(screen_width,screen_height,screen_bpp,SDL_SWSURFACE|SDL_FULLSCREEN);
    else
        screen = SDL_SetVideoMode(screen_width,screen_height,screen_bpp,SDL_SWSURFACE);
    SDL_WM_SetCaption(window_caption, NULL);
}

// Complete path to file from folder, filename and suffix
char *complete_file_path(char *folder,char *filename,char *suffix) {
    char *path;
    path = new char[strlen(folder)+strlen(filename)+strlen(suffix)+1];
    strcpy(path,folder);
    strcat(path,filename);
    strcat(path,suffix);
    return path;
}

// Load image
SDL_Surface *load_image( std::string filename) {
    SDL_Surface *loaded_image = NULL;
    SDL_Surface *optimized_image = NULL;
    loaded_image = IMG_Load(filename.c_str());
    if (loaded_image != NULL) {
        optimized_image = SDL_DisplayFormatAlpha(loaded_image);
        SDL_FreeSurface(loaded_image);
    }
    return optimized_image;
}

// Put part of surface to another surface
void blit_surface(SDL_Surface *from, SDL_Surface *to,int x1,int y1,int w,int h,int x2,int y2) {
    SDL_Rect rect1;
    rect1.x = x1;
    rect1.y = y1;
    rect1.w = w;
    rect1.h = h;
    SDL_Rect rect2;
    rect2.x = x2;
    rect2.y = y2;
    SDL_BlitSurface(from, &rect1, to, &rect2);
}
