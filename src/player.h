#ifndef _PLAYER_H
#define _PLAYER_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "config.h"
#include "gamelib.h"
#include "camera.h"

class Player {
    private:
    int max_speed;
    bool jump;
    int jump_lenght;
    
    public:
    SDL_Surface *character;
    // Position
    int x;
    int y;
    // Moving velocity
    int velx;
    int vely;
    // Size
    int w;
    int h;
    
    void init(int, int); // player initialization

    void control(Uint8*); // increasing velocity
        
    void move(Camera*); // moving player

    void draw(Camera*); // draw player

    void collision(int[400][400]); // solve collisions
};

#endif
