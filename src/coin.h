#ifndef _COIN_H
#define _COIN_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "config.h"
#include "gamelib.h"

class Coin {
    private:
        int x;
        int y;
        int collision[1];
    public:
    void init(int, int, int, int);
    
    void draw();
};

extern Coin coins[500];

#endif
