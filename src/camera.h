#ifndef _CAMERA_H
#define _CAMERA_H

#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <iostream>
#include "config.h"

class Camera {
    private:
    
    public:
    int x;
    int y;
    int w;
    int h;
    
    void init(int sx, int sy, int sw, int sh);
};

#endif
